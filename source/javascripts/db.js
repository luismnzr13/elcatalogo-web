// BEGIN ProductsApi
const AIRTABLE_ID = "appx2rNBDVAw3AqVt";
const AIRTABLE_API_KEY = "keyzi6J3HUqjxUbX8";
const BASE_URL =
  "https://api.airtable.com/v0/" +
  AIRTABLE_ID +
  "/Producto%201?view=Grid%20view";

class ProductsApi {
  static fetchProducts(filterByFormula) {
    return fetch(BASE_URL + filterByFormula, {
      headers: {
        Authorization: `Bearer ${AIRTABLE_API_KEY}`
      }
    })
      .then(this.json)
      .then(data => {
        return this.convertDataToProducts(data);
        console.log("Request succeeded with JSON response", data);
      })
      .catch(function(error) {
        console.log("Request failed", error);
      });
  }

  static convertDataToProducts(data) {
    return data.records.map(record => {
      return this.convertRecordToProduct(record);
    });
  }

  static convertRecordToProduct(record) {
    const fields = record.fields;
    return {
      title: fields["Nombre"],
      imageUrl: this.getProductImage("Producto", fields)
    };
  }

  static json(response) {
    return response.json();
  }

  static getProductImage(productName, fields) {
    if (fields[`${productName} Image`] && fields[`${productName} Image`][0]) {
      return fields[`${productName} Image`][0].url;
    }
  }
}
// END ProductsApi

// BEGIN product markup
// productData = { name, team_a, team_b }
const getProductMarkup = product => {
  return `<div class="column-cooler__display">
            <img class="column-cooler__img" src="${product.imageUrl}" alt="">
          </div>
          `;
};
// END product markup

const loadProducts = (filterByFormula = "") => {
  ProductsApi.fetchProducts(filterByFormula)
    .then(products => {
      $("div.column-cooler__carousel").html("");
      products.forEach(product => {
        $("div.column-cooler__carousel").append(getProductMarkup(product));
      });
    })
    .then(() => {
      $(".column-cooler__carousel").slick();
    })
    .catch(error => {
      console.error("error: ProductsApi", error);
    });
};

loadProducts();
