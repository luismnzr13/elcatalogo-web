const BASE_URL2 =
  "https://api.airtable.com/v0/" +
  AIRTABLE_ID +
  "/Producto%202?view=Grid%20view";

class ProductsTwoApi {
  static fetchProducts(filterByFormula) {
    return fetch(BASE_URL2 + filterByFormula, {
      headers: {
        Authorization: `Bearer ${AIRTABLE_API_KEY}`
      }
    })
      .then(this.json)
      .then(data => {
        return this.convertDataToProducts(data);
        console.log("Request succeeded with JSON response", data);
      })
      .catch(function(error) {
        console.log("Request failed", error);
      });
  }

  static convertDataToProducts(data) {
    return data.records.map(record => {
      return this.convertRecordToProduct(record);
    });
  }

  static convertRecordToProduct(record) {
    const fields = record.fields;
    return {
      title: fields["Nombre"],
      imageUrlTwo: this.getProductImage("Bolsa", fields)
    };
  }

  static json(response) {
    return response.json();
  }

  static getProductImage(productName, fields) {
    if (fields[`${productName} Image`] && fields[`${productName} Image`][0]) {
      return fields[`${productName} Image`][0].url;
    }
  }
}
// END ProductsApi

// BEGIN product markup
// productData = { name, team_a, team_b }
const getProductTwoMarkup = product => {
  return `<div class="column-cooler__display">
            <img class="column-cooler__img" src="${product.imageUrlTwo}" alt="">
          </div>
          `;
};
// END product markup

const loadProductsTwo = (filterByFormula = "") => {
  ProductsTwoApi.fetchProducts(filterByFormula)
    .then(products => {
      $("div.column-cooler__carousel-2").html("");
      products.forEach(product => {
        $("div.column-cooler__carousel-2").append(getProductTwoMarkup(product));
      });

      // Bind on click action to expand article.
    })
    .then(() => {
      $(".column-cooler__carousel-2").slick();
    })
    .catch(error => {
      console.error("error: ProductsApi", error);
    });
};

loadProductsTwo();
