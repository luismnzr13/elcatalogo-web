const AIRTABLE_ID = "appx2rNBDVAw3AqVt";
const AIRTABLE_API_KEY = "keyzi6J3HUqjxUbX8";
const BASE_URL4 = "https://api.airtable.com/v0/" + AIRTABLE_ID + "/LandEnglish";

class CoversEngApi {
  static fetchCovers(filterByFormula) {
    return fetch(BASE_URL4 + filterByFormula, {
      headers: {
        Authorization: `Bearer ${AIRTABLE_API_KEY}`
      }
    })
      .then(this.json)
      .then(data => {
        return this.convertDataToCovers(data);
        console.log("Request succeeded with JSON response", data);
      })
      .catch(function(error) {
        console.log("Request failed", error);
      });
  }

  static convertDataToCovers(data) {
    return data.records.map(record => {
      return this.convertRecordToCover(record);
    });
  }

  static convertRecordToCover(record) {
    const fields = record.fields;
    return {
      quoteOne: fields["Quote one"],
      quoteOneColor: fields["Color one"],
      quoteTwo: fields["Quote two"],
      quoteTwoColor: fields["Color two"],
      quoteThree: fields["Quote three"],
      quoteThreeColor: fields["Color three"],
      coverOne: {
        imageUrlCover: this.getCoverImage("BG1", fields)
      },
      coverTwo: {
        imageUrlCoverTwo: this.getCoverImage("BG2", fields)
      },
      coverThree: {
        imageUrlCoverThree: this.getCoverImage("BG3", fields)
      }
    };
  }

  static json(response) {
    return response.json();
  }

  static getCoverImage(coverName, fields) {
    if (fields[`${coverName} Image`] && fields[`${coverName} Image`][0]) {
      return fields[`${coverName} Image`][0].url;
    }
  }
}

// BEGIN cover markup
// coverData = { name, team_a, team_b }
const getCoverEngMarkup = cover => {
  return `<div class="hero is-success is-fullheight-with-navbar">
    <div class="hero-body">
      <div class="container hero-main">
        <h1 class="hero-main__title title">
          Reusable bags & coolers <br>
          <span class="hero-main__titleItalic-En">custom made</span> with your <span class="hero-main__titleBold">design</span>
        </h1>
      </div>
    </div>
  </div>
  <div class="hero is-second is-fullheight-with-navbar plydex" style="background-image: url(${cover.coverOne.imageUrlCover})">
    <div class="hero-body plydex__hero-body">
        <div class="container has-text-centered plydex__land">
          <h3 class="title plydex-title plydex-carousel" style="color: ${cover.quoteOneColor};">
            ${cover.quoteOne}
          </h3>
        </div>
      </div>
  </div>
  <div class="hero is-fullheight-with-navbar reusableBags-cover" style="background-image: url(${cover.coverTwo.imageUrlCoverTwo})">
    <div class="container has-text-centered">
      <h3 class="title reusableBags__titleAlt" style="color: ${cover.quoteTwoColor};">
        ${cover.quoteTwo}
      </h3>
    </div>
  </div>
  <div class="hero is-fullheight-with-navbar giftBags-cover" style="background-image: url(${cover.coverThree.imageUrlCoverThree})">
    <div class="container has-text-centered">
      <h3 class="title reusableBags__titleAlt" style="color: ${cover.quoteThreeColor};">
        ${cover.quoteThree}
      </h3>
    </div>
  </div>
          `;
};
// END cover markup

const loadCoversEng = (filterByFormula = "") => {
  CoversEngApi.fetchCovers(filterByFormula)
    .then(covers => {
      $("div.main-carousel.hero.is-fullheight-with-navbar").html("");
      covers.forEach(cover => {
        $("div.main-carousel.hero.is-fullheight-with-navbar").append(
          getCoverEngMarkup(cover)
        );
      });
    })
    .then(() => {
      $(".main-carousel").slick({
        autoplay: true,
        autoplaySpeed: 3500,
        dots: true
      });
    })
    .catch(error => {
      console.error("error: CoversApi", error);
    });
};

loadCoversEng();
